import numpy as np
import pandas as pd
from scipy.stats import mode
from sklearn.tree import DecisionTreeRegressor

TREE_COEF = 0.2


class GradientBoostingMachine:
    def __init__(self):
        self.trees = []
        self.start_approximation = None
        self.first_class = None

    def fit(self, X, y, n_iteration=4, tree_depth=2):
        self.start_approximation = 0
        y_numpy = np.reshape(y.to_numpy(dtype="int"), (X.shape[0], 1))
        self.first_class = y_numpy[0]
        print('First class:' + str(self.first_class))

        for i in range(1, n_iteration):
            predicts = self.predicts_without_logit(X)
            r = -self.grad(predicts, y_numpy)
            tree = DecisionTreeRegressor(max_depth=tree_depth)
            tree.fit(X, r)
            self.trees.append(tree)

    def grad(self, predicts, y):
        return 2 * y / (1 + np.exp(2 * y * predicts))

    def predicts_without_logit(self, data):
        predicts = np.zeros((data.shape[0], 1))
        predicts += self.start_approximation
        for i in range(len(self.trees)):
            tree_predict = TREE_COEF * self.trees[i].predict(data)
            predicts += np.reshape(tree_predict, (predicts.shape[0], 1))
        return predicts

    def sigmoid(self, x):
        return 1 / (1 + np.exp(-x))

    def predict_proba(self, data):
        proba = self.sigmoid(self.predicts_without_logit(data))
        return np.concatenate((proba, 1 - proba), axis=1)

    def predict(self, data):
        proba = self.sigmoid(self.predicts_without_logit(data))[:, 0]
        for i in range(proba.shape[0]):
            if (proba[i] < 0.5):
                proba[i] = self.first_class * -1
            else:
                proba[i] = self.first_class

        return proba
