import numpy as np
import pandas as pd
from scipy.stats import mode
from sklearn.tree import DecisionTreeClassifier


class RandomForest:
    def __init__(self, number_of_trees):
        self.number_of_trees = number_of_trees
        self.forest = []
        self.attributes_indices = []

    def fit(self, attributes, target, n_samples, number_of_features_for_tree=-1):

        if (number_of_features_for_tree == -1):
            number_of_features_for_tree = int(np.sqrt(attributes.shape[1]))

        assert n_samples > 0
        assert number_of_features_for_tree > 0 and number_of_features_for_tree <= attributes.shape[1]

        for i in range(0, self.number_of_trees):
            sample_attributes, sample_target, attribute_indices = self.get_sample(attributes, target,
                                                                                  number_of_features_for_tree,
                                                                                  n_samples)
            tree = DecisionTreeClassifier(max_leaf_nodes=3)
            tree.fit(sample_attributes, sample_target)
            self.attributes_indices.append(attribute_indices)
            self.forest.append(tree)

    def get_sample(self, attributes, target, number_of_features, n_samples):
        row_indices = np.random.randint(0, attributes.shape[0], n_samples)
        column_indices = np.random.randint(0, attributes.shape[1], number_of_features)

        bootstrap_attributes = attributes.iloc[row_indices, column_indices]
        bootstrap_target = target.iloc[row_indices]
        return bootstrap_attributes, bootstrap_target, column_indices

    def predict(self, data):
        predicts = [self.forest[i].predict(data.iloc[:, self.attributes_indices[i]]) for i in
                    range(0, len(self.forest))]
        numpy_predicts = np.array(predicts)
        numpy_predicts = np.transpose(numpy_predicts)
        return np.array([mode(numpy_predicts[i, :])[0][0] for i in range(numpy_predicts.shape[0])])

    def predict_proba(self, data):
        predicts = [self.forest[i].predict_proba(data.iloc[:, self.attributes_indices[i]]) for i in
                    range(0, len(self.forest))]
        numpy_predicts = np.array(predicts)
        pred_sum = np.sum(numpy_predicts, axis=0, keepdims=True) / (numpy_predicts.shape[0])
        return [(pred_sum[:, i, 0], pred_sum[:, i, 1]) for i in range(pred_sum.shape[1])]

