import numpy as np
import pandas as pd

class DecisionTree:
    def __init__(self, level=1, max_level=None):
        self.level = level
        self.max_level = max_level
        self.left_subtree = None
        self.right_subtree = None
        self.attribute_name = None
        self.thresh = None
        self.predict_class = None
        self.value_counts = None

    def fit(self, attributes, target, min_count_in_leaf=1):
        stop_condition = target.shape[0] <= min_count_in_leaf or target.value_counts().shape[
            0] == 1 or self.level == self.max_level
        if (stop_condition):
            self.create_leaf(target)
            return

        max_ig = -1

        for attribute_name in attributes.columns:
            step = self.calculate_step(attributes, attribute_name)

            max_ig_for_attribute = -1
            best_thresh = -1

            if (step == 0.0):
                continue

            for thresh in np.linspace(attributes[attribute_name].min(), attributes[attribute_name].max(), 5,
                                      endpoint=False):
                ig = self.calculate_information_gain(attributes, target, attribute_name, thresh)
                if (ig > max_ig_for_attribute or max_ig_for_attribute == -1):
                    max_ig_for_attribute = ig
                    best_thresh = thresh

            if (max_ig_for_attribute < max_ig or max_ig == -1):
                max_ig = max_ig_for_attribute
                self.thresh = best_thresh
                self.attribute_name = attribute_name

        if (self.thresh == None or self.attribute_name == None):
            self.create_leaf(target)
            return

        left_tree_attributes, left_tree_target, right_tree_attributes, right_tree_target = self.split_data(attributes,
                                                                                                           target,
                                                                                                           self.attribute_name,
                                                                                                           self.thresh)
        if (left_tree_attributes.shape[0] != 0):
            self.left_subtree = DecisionTree(self.level + 1, self.max_level)
            self.left_subtree.fit(left_tree_attributes, left_tree_target, min_count_in_leaf)

        if (right_tree_attributes.shape[0] != 0):
            self.right_subtree = DecisionTree(self.level + 1, self.max_level)
            self.right_subtree.fit(right_tree_attributes, right_tree_target, min_count_in_leaf)

        return max_ig

    def create_leaf(self, target):
        self.predict_class = target.value_counts().index.to_list()[0]

        if (target.value_counts().shape[0] == 1):
            if (self.predict_class == 0):
                self.value_counts = (1.0, 0)
            if (self.predict_class == 1):
                self.value_counts = (0.0, 1.0)
        else:
            class_0 = target[target == 0].shape[0]
            class_1 = target[target == 1].shape[0]
            total_count = target.shape[0]
            self.value_counts = (class_0 / total_count, class_1 / total_count)

    def calculate_step(self, attributes, attribute_name):
        min_value = attributes[attribute_name].min()
        max_value = attributes[attribute_name].max()
        return (max_value - min_value) / 5

    def calculate_information_gain(self, attributes, target, attribute_name, thresh):
        S_0 = self.calculate_entropy(target)

        left_tree_attributes, left_tree_target, right_tree_attributes, right_tree_target = self.split_data(attributes,
                                                                                                           target,
                                                                                                           attribute_name,
                                                                                                           thresh)
        left_entropy = self.calculate_entropy(left_tree_target)
        right_entropy = self.calculate_entropy(right_tree_target)

        left_weight = left_tree_target.shape[0] / target.shape[0]
        right_weight = right_tree_target.shape[0] / target.shape[0]

        return S_0 - left_weight * left_entropy - right_weight * right_entropy

    def split_data(self, attributes, target, attribute_name, thresh):
        left_condition = attributes[attribute_name] <= thresh
        right_condition = attributes[attribute_name] > thresh

        left_tree_attributes = attributes[left_condition]
        left_tree_target = target[left_condition]

        right_tree_attributes = attributes.loc[right_condition]
        right_tree_target = target.loc[right_condition]
        return left_tree_attributes, left_tree_target, right_tree_attributes, right_tree_target

    def calculate_entropy(self, target):
        proportions = target.value_counts(normalize=True)
        return -np.sum(proportions.iloc[:] * np.log2(proportions.iloc[:]))

    def predict(self, row):
        if (self.predict_class != None):
            return self.predict_class
        else:
            if (row[self.attribute_name] <= self.thresh):
                return self.left_subtree.predict(row)
            else:
                return self.right_subtree.predict(row)

    def predict_proba(self, row):
        if (self.predict_class != None):
            return self.value_counts
        else:
            if (row[self.attribute_name] <= self.thresh):
                return self.left_subtree.predict_proba(row)
            else:
                return self.right_subtree.predict_proba(row)
