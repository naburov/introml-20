import matplotlib.pyplot as plt
from graphviz import Digraph, nohtml
import random

def roc_curve(labels, value_counts):
    assert len(labels) == len(value_counts)

    label_value_tuples = zip(labels, value_counts)
    label_value_tuples = sorted(label_value_tuples, key=lambda x: x[1][0])

    P = sum([labels[i] == 1 for i in range(0, len(labels))])
    N = sum([labels[i] == 0 for i in range(0, len(labels))])

    fpr = [0]
    tpr = [0]

    right_step = 1 / N
    up_step = 1 / P

    for i in range(1, len(label_value_tuples)):
        if (label_value_tuples[i][0] == 1):
            fpr.append(fpr[i - 1] + up_step)
            tpr.append(tpr[i - 1])
        else:
            fpr.append(fpr[i - 1])
            tpr.append(tpr[i - 1] + right_step)
    return fpr, tpr


def train_test_split(attributes, target, train=0.7):
    assert attributes.shape[0] == target.shape[0]

    rows = attributes.shape[0]
    indices = [i for i in range(0, rows - 1)]
    random.shuffle(indices)

    train_split = int(train * len(indices))
    train_indices = indices[:train_split]
    test_indices = indices[train_split + 1:]

    attributes_train = attributes.iloc[train_indices, :]
    target_train = target.iloc[train_indices]

    attributes_test = attributes.iloc[test_indices, :]
    target_test = target.iloc[test_indices]
    return attributes_train, attributes_test, target_train, target_test


def draw_tree(tree, e, parent_node_name):
    if tree.predict_class is not None:
        node_name = 'Class: ' + str(tree.predict_class)
        node_id = str(random.randint(0, 100000000))
        e.node(node_id, label=node_name)
        e.edge(parent_node_name, node_id, len='1.00')
        return
    else:
        node_name = str(tree.attribute_name) + ', ' + str(tree.thresh)
        node_id = str(random.randint(0, 100000000))
        e.node(node_id, label=nohtml(node_name))
        if parent_node_name != '':
            e.edge(parent_node_name, node_id, len='1.00')
        if tree.left_subtree is not None:
            draw_tree(tree.left_subtree, e, node_id)
        if tree.right_subtree is not None:
            draw_tree(tree.right_subtree, e, node_id)
    return 0


def visualize_tree(tree):
    e = Digraph('tree', filename='er.gv', node_attr={'shape': 'record', 'height': '.1'})
    draw_tree(tree, e, '')
    return e

def predictRND(data):
    predicts_labels = [-1 if random.randint(0,2) else 1 for i in range(data.shape[0])]
    predict_proba_tuples = [(0.5, 0.5) for i in range(data.shape[0])]
    return predicts_labels, predict_proba_tuples
